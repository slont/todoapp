<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
  crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
  <form method="post" action="/TodoApp/app">
    <div>
      <input type="text" name="task" placeholder="Input task here!" />
      <button type="submit" name="register_task">Register</button>
    </div>
    <div>
      <ul>
        <%
          Map<String, String> task_map = (LinkedHashMap<String, String>) request.getAttribute("task_map");
          Map<String, String> checked_map = (HashMap<String, String>) request.getAttribute("checked_map");
          try {
            List<String> keys = new ArrayList(task_map.keySet());
            Collections.reverse(keys);
            for (String key : keys) {
              String value = task_map.get(key);
              String checked = checked_map.get(key);
        %>
        <li>
          <input type="checkbox" <%=checked%> />
          <span><%=value%></span>
          <button type="submit" name="delete_task" value="<%=key%>" class="btn btn-default">
            <span class="glyphicon glyphicon-remove"></span>
          </button>
        </li>
        <%
          }
          } catch (NullPointerException e) {
          }
        %>
      </ul>
    </div>
    <div>
      <span><button type="submit" name="select_all_task">Select All</button></span>
      <span><button type="submit" name="delete_selected_tasks">Delete Selected</button></span>
    </div>
  </form>
</body>
</html>