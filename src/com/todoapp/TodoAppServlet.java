package com.todoapp;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TodoAppServlet
 */
@WebServlet("/app")
public class TodoAppServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private MessageDigest md5 = null;

    private Map<String, String> task_map = new LinkedHashMap<>();
    private Map<String, String> checked_map = new HashMap<>();


    /**
     * @see HttpServlet#HttpServlet()
     */
    public TodoAppServlet() {
        super();
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String task = request.getParameter("task");
        System.out.println(task);
        response.sendRedirect("app.jsp");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (null != request.getParameter("register_task")) {
            registerTask(request, response);
        } else if(null != request.getParameter("delete_task")) {
            deleteTask(request, response);
        } else if(null != request.getParameter("select_all_task")) {
            selectAllTask(request, response);
        } else if(null != request.getParameter("delete_selected_tasks")) {
            deleteSelectedTasks(request, response);
        }
    }

    private void registerTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String task = validate(request.getParameter("task"));
        if (!task.isEmpty()) {
            BigInteger hex = new BigInteger(md5.digest((String.valueOf(System.currentTimeMillis()) + task).getBytes()));
            task_map.put(hex.toString(), task);
            checked_map.put(hex.toString(), "");
        }
        forwardAppJsp(request, response);

    }

    private void deleteTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String key = validate(request.getParameter("delete_task"));
        task_map.remove(key);
        checked_map.remove(key);
        forwardAppJsp(request, response);
    }

    private void deleteSelectedTasks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Set<Map.Entry<String, String>> deleteSet = new HashSet<>(checked_map.entrySet());
        for(Map.Entry<String, String> entry: deleteSet) {
            if(!entry.getValue().isEmpty()) {
                task_map.remove(entry.getKey());
                checked_map.remove(entry.getKey());
            }
        }
        forwardAppJsp(request, response);
    }

    private void selectAllTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        for(Map.Entry<String, String> entry: checked_map.entrySet()) {
            checked_map.put(entry.getKey(), "checked");
        }
        forwardAppJsp(request, response);
    }

    private void selectTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String key = validate(request.getParameter("select_task"));
        checked_map.put(key, checked_map.get(key) == "" ? "checked" : "");
        forwardAppJsp(request, response);
    }

    private void forwardAppJsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("task_map", task_map);
        request.setAttribute("checked_map", checked_map);
        getServletContext().getRequestDispatcher("/app.jsp").forward(request, response);
    }

    private String validate(String str) {
        return null == str ? "" : str;
    }
}
